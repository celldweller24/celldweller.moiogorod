<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/censure/Censure.php");

CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;

//AddEventHandler("main", "OnBeforeUserAdd", Array("MailToLogin", "OnBeforeUserAddHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("Geolocation", "OnBeforeIBlockElementAddHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("Geolocation", "updateCatalogElement"));
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("Ads", "OnAfterIBlockElementUpdateHandler")); // add to highloadblock

/*class MailToLogin{ // создаем обработчик события "OnBeforeUserAdd"
    function OnBeforeUserAddHandler(&$arFields)
    {

    }
}*/

class Geolocation // создаем обработчик события "OnAfterIBlockElementAdd"
{
    function OnBeforeIBlockElementAddHandler(&$arFields)
    {
        if($arFields['IBLOCK_ID'] == 1)
        {
            $arFields['PROPERTY_VALUES'][24] = ConvertTimeStamp(time(), 'FULL');

            if($arFields['PROPERTY_VALUES'][1]['VALUE']){
                $location = $arFields['PROPERTY_VALUES'][1]['VALUE'];
            }
            else{
                $location = reset($arFields['PROPERTY_VALUES']['1']);
                $location = $location['VALUE'];
            }

            if($location){
                $locationArray = getLocation($location);

                $arFields['PROPERTY_VALUES'][14] = $locationArray['LocalityName'];
                $arFields['PROPERTY_VALUES'][15] = $locationArray['SubAdministrativeAreaName'];
                $arFields['PROPERTY_VALUES'][16] = $locationArray['AdministrativeAreaName'];
            }
        }
    }

    public function updateCatalogElement($arFields)
    {
        if($arFields['IBLOCK_ID'] == 1)
        {
            $element = new CIBlockElement;

            // last update
            $element->SetPropertyValuesEx($arFields['ID'], 1, array('LAST_UPDATE' => ConvertTimeStamp(time(), 'FULL')));

            // geolocation
            $res = CIBlockElement::GetProperty(1, $arFields['ID'], "sort", "asc", array("CODE" => "YANDEX_MAP"));
            $locationCurrent = null;
            if ($ob = $res->GetNext()){
                $locationCurrent = $ob['VALUE'];

                $locationArray = getLocation($locationCurrent);

                $element->SetPropertyValuesEx($arFields['ID'], 1, array('VENDOR_CITY' => $locationArray['LocalityName']));
                $element->SetPropertyValuesEx($arFields['ID'], 1, array('VENDOR_SUBAREA' => $locationArray['SubAdministrativeAreaName']));
                $element->SetPropertyValuesEx($arFields['ID'], 1, array('VENDOR_AREA' => $locationArray['AdministrativeAreaName']));
            }
        }
    }
}

function getLocation($coordinats){ // get city, administrative area and subadministrative area from coordinats
    if($coordinats)
    {
        $location = explode(',', $coordinats);

        $url_yandex = "https://geocode-maps.yandex.ru/1.x/?kind=locality&format=json&geocode=".$location[1].", ".$location[0];
        $data = file_get_contents($url_yandex);
        $ar_city = json_decode($data);

        if($ar_city){
            $geo_obj = $ar_city->response->GeoObjectCollection->featureMember[0]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AdministrativeArea;

            /*$message = "<pre>".print_r($geo_obj, true)."</pre>";
            AddMessage2Log($message);*/

            $AdministrativeAreaName = '';
            $SubAdministrativeAreaName = '';
            $LocalityName = '';

            $AdministrativeAreaName = $geo_obj->AdministrativeAreaName; // Область (Москва, Московская область)

            if($geo_obj->SubAdministrativeArea){
                $SubAdministrativeAreaName = $geo_obj->SubAdministrativeArea->SubAdministrativeAreaName; // Район
                $LocalityName              = $geo_obj->SubAdministrativeArea->Locality->LocalityName; // Населенный пункт
            }
            else{
                $SubAdministrativeAreaName = ''; // Район
                $LocalityName              = $geo_obj->Locality->LocalityName; // Населенный пункт
            }

            if($LocalityName == $AdministrativeAreaName){
                $LocalityName = '';
            }

            $result['AdministrativeAreaName'] = $AdministrativeAreaName;
            $result['SubAdministrativeAreaName'] = $SubAdministrativeAreaName;
            $result['LocalityName'] = $LocalityName;

            return $result;
        }
        else{
            mail("kuh.dmitry@gmail.com", 'moiogorod', 'not connect to yandex'); //Отправляем письмо
            return false;
        }
    }

    return false;
}

class Ads // класс для работы с объявлениями
{

    function OnAfterIBlockElementUpdateHandler($arFields) // добавление в хайлод типа товара для статистики
    {
        $hlBlockId = 1;
        //AddMessage2Log($arFields['PROPERTY_VALUES']);

        if($arFields["ACTIVE"] == 'Y')
        {
            $itemType = trim($arFields['PROPERTY_VALUES']['5'][$arFields['ID'].':5']['VALUE']);
            $itemSection = trim($arFields['IBLOCK_SECTION'][0]);

            $hlblock = HLBT::getById($hlBlockId)->fetch();
            $entity = HLBT::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();

            $rsData = $entity_data_class::getList(array(
                'select' => array('ID', 'UF_ITEM_TYPE', 'UF_CATALOG_SECTION'),
                'filter' => array('=UF_ACTIVE' => 1, 'UF_ITEM_TYPE' => $itemType)
            ));

            if(!$rsData->fetch()){
                $data = array(
                    "UF_ACTIVE" => 1,
                    "UF_ITEM_TYPE" => $itemType,
                    "UF_CATALOG_SECTION" => $itemSection,
                );
                $entity_data_class::add($data);
            }
        }
    }
}

// Get class name for Highload Block
function GetEntityDataClass($HlBlockId) {
    if (empty($HlBlockId) || $HlBlockId < 1)
    {
        return false;
    }
    $hlblock = HLBT::getById($HlBlockId)->fetch();
    $entity = HLBT::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();
    return $entity_data_class;
}

function debug($arr)
{
    global $debug;
    $debug[] = $arr;
}
function dump($arr)
{
    echo '<pre style="background: #000; color: greenyellow; padding: 15px; font-size: 12px">'.print_r($arr, true).'</pre>';
}

/**
 * Get bootstrap grid regarding of element quantity
 */
function getBootstrapGrid($array) {
    if (!empty($array)) {
        $col_wrapper = array();

        /* array item quantity => col number */
        $col_wrapper = array(
            6 => 2,
            4 => 3,
            3 => 4,
            2 => 6,
            1 => 12
        );
        $arary_cout = count($array);

        foreach ($col_wrapper as $element_amount => $col_bootstrap) {
            $result = array();
            if ($arary_cout >= $element_amount) {
                $row_amount = $arary_cout / $element_amount;
                $recidue = $arary_cout % $element_amount;

                if ($recidue != 0) {
                    $row_amount += 1;
                }

                $result = array(
                    'row_amount' => intval($row_amount),
                    'col_amount' => $element_amount,
                    'col_index' => $col_bootstrap
                );
                return $result;
            }
        }
    }
}

function getPreviousPage() {
    global $APPLICATION;
    $current_page = explode('/', $APPLICATION->GetCurPage());
    $clean_url_array = array();
    foreach ($current_page as $value) {
        if (!empty($value)) {
            $clean_url_array[] = $value;
        }
    }
    array_pop($clean_url_array);
    $previous_url = implode('/', $clean_url_array);
    $previous_url = '/' . $previous_url . '/';
    return $previous_url;
}

/**
 * Auxiliary function for geting of difference date of publication
 */
function getPublicationWord($number, $suffix) {
    $keys = array(2, 0, 1, 1, 1, 2);
    $mod = $number % 100;
    $suffix_key = ($mod > 7 && $mod < 20) ? 2: $keys[min($mod % 10, 5)];
    return $number . ' ' . $suffix[$suffix_key] . ' назад';
}

/**
 * Getting value of the difference date of publication
 */
function getPublicationDate($date) {
    $advert_date = new DateTime($date);
    $current_date = new DateTime(date('d-m-Y H:i:s'));
    $diff = $current_date->diff($advert_date);

    $words_array = [];
    if ($diff->y) {
        $words_array = array('год', 'года', 'лет');
        return getPublicationWord($diff->y, $words_array);
    } elseif ($diff->m) {
        $words_array = array('месяц', 'месяца', 'месяцев');
        return getPublicationWord($diff->m, $words_array);
    } elseif ($diff->d) {
        $words_array = array('день', 'дня', 'дней');
        return getPublicationWord($diff->d, $words_array);
    } elseif ($diff->h) {
        $words_array = array('час', 'часа', 'часов');
        return getPublicationWord($diff->h, $words_array);
    } elseif ($diff->i) {
        $words_array = array('минута', 'минуты', 'минут');
        return getPublicationWord($diff->i, $words_array);
    } elseif ($diff->s) {
        return 'меньше минуты назад';
    }
}

// Counting distance between coordinats
function getCoordDistance($lat1, $lon1, $lat2, $lon2) {
    $lat1 *= M_PI / 180;
    $lat2 *= M_PI / 180;
    $lon1 *= M_PI / 180;
    $lon2 *= M_PI / 180;

    $d_lon = $lon1 - $lon2;

    $slat1 = sin($lat1);
    $slat2 = sin($lat2);
    $clat1 = cos($lat1);
    $clat2 = cos($lat2);
    $sdelt = sin($d_lon);
    $cdelt = cos($d_lon);

    $y = pow($clat2 * $sdelt, 2) + pow($clat1 * $slat2 - $slat1 * $clat2 * $cdelt, 2);
    $x = $slat1 * $slat2 + $clat1 * $clat2 * $cdelt;

    return atan2(sqrt($y), $x) * 6372795;
}

// Get current menu category from url
function currentMenuCategory($url) {
    $url_explode = explode('/', $url);
    return $url_explode[2];
}

function checkAnonymRedirect(){
    global $USER;
    if (!$USER->IsAuthorized()){
        LocalRedirect("/account/login/");
    }
}

function checkAuthRedirect(){
    global $USER;
    if ($USER->IsAuthorized()){
        LocalRedirect("/account/advertisement/");
    }
}

// Get all properties of item.
function getAllElementProperty($element_id) {
    $elementObj = new CIBlockElement();

    $arSelect = array("ID", "IBLOCK_ID", "PROPERTY_*");
    $arFilter = array("IBLOCK_ID" => 1, "ID" => $element_id, "PROPERTY_ITEM_USER" => CUser::GetID());
    $result = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
    $result = $result->GetNextElement();

    $propertiesArray = [];
    foreach ($result->props as $property) {
        $prorertyId = $property["ID"];
        if (array_key_exists("PROPERTY_" . $prorertyId, $result->fields)) {
            if (strpos($result->fields["PROPERTY_" . $prorertyId], '.000')) {
                $propertyValue = round((integer)$result->fields["PROPERTY_" . $prorertyId]);
            } else {
                $propertyValue = $result->fields["PROPERTY_" . $prorertyId];
            }
            $propertiesArray[$property["CODE"]] = $propertyValue;
        }
    }
    return $propertiesArray;
}