<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Page\Asset;

// Checking registrarion confirm.
if (!strpos($APPLICATION->GetCurPage(), 'confirm')) {
  $curentUser = CUser::GetByID(CUser::GetID())->Fetch();
  if (!empty($curentUser)) {
    if (!$curentUser['CONFIRM_CODE_SUCCESS']) {
      LocalRedirect('/account/confirm/');
    }
  }
}

?>
<!DOCTYPE html>
<html>
<head lang="ru">
  <? $APPLICATION->ShowHead(); ?>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title><? $APPLICATION->ShowTitle() ?></title>

  <!-- CSS  -->
  <? Asset::getInstance()->addString('<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&amp;subset=cyrillic" rel="stylesheet">'); ?>
  <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/bootstrap.css"); ?>
  <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/reset.css"); ?>
  <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/nik.css"); ?>
  <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/dima.css"); ?>
  <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/responsive.css"); ?>
  <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/bootstrap-select.css"); ?>
  <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/jquery.bxslider.css"); ?>
  <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/jquery.fancybox.css"); ?>
  <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/animate.css"); ?>
  <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/fonts/font-awesome.min.css"); ?>

  <!--JS -->
  <? Asset::getInstance()->addJs("//code.jquery.com/jquery-1.9.1.js"); ?>
  <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/bootstrap.js"); ?>
  <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/bootstrap-select.min.js"); ?>
  <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.bxslider.js"); ?>
  <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.fancybox.js"); ?>
  <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.maskedinput.min.js"); ?>
  <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/main.js"); ?>
  <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/nik.js"); ?>
  <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/dima.js"); ?>
  <!-- END JS -->

  <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>

  <!-- Yandex.Metrika counter -->
  <script type="text/javascript" >
      (function (d, w, c) {
          (w[c] = w[c] || []).push(function() {
              try {
                  w.yaCounter49029740 = new Ya.Metrika2({
                      id:49029740,
                      clickmap:true,
                      trackLinks:true,
                      accurateTrackBounce:true,
                      webvisor:true
                  });
              } catch(e) { }
          });

          var n = d.getElementsByTagName("script")[0],
              s = d.createElement("script"),
              f = function () { n.parentNode.insertBefore(s, n); };
          s.type = "text/javascript";
          s.async = true;
          s.src = "https://mc.yandex.ru/metrika/tag.js";

          if (w.opera == "[object Opera]") {
              d.addEventListener("DOMContentLoaded", f, false);
          } else { f(); }
      })(document, window, "yandex_metrika_callbacks2");
  </script>
  <noscript><div><img src="https://mc.yandex.ru/watch/49029740" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
  <!-- /Yandex.Metrika counter -->

</head>
<body>
<div id="panel">
  <? $APPLICATION->ShowPanel(); ?>
</div>
<header>
  <div class="wrapper container"> <!-- header wrapper end -->
    <div class="header-control row">
      <div class="menu-button-responsive hidden-lg hidden-md col-sm-1 col-xs-2">
        <p class="menu-control-icon"></p>
      </div>
      <!--logo-->
      <div class="logo col-lg-3 col-md-3 col-sm-2 col-xs-4">
        <a href="/"><img src="<?= SITE_TEMPLATE_PATH ?>/images/logo_icon.png"></a>
      </div>
      <!--search-->
      <div class="col-lg-4 col-md-4 col-sm-5 hidden-xs">
        <div id="search-form-global desktop" class="global-search">
          <? $APPLICATION->IncludeComponent("bitrix:search.title", "header.search.title", Array(
            "CATEGORY_0" => array(  // Ограничение области поиска
              0 => "iblock_catalog",
            ),
            "CATEGORY_0_TITLE" => "Объявления",  // Название категории
            "CATEGORY_0_iblock_catalog" => array(  // Искать в информационных блоках типа "iblock_catalog"
              0 => "all",
            ),
            "CHECK_DATES" => "N",  // Искать только в активных по дате документах
            "CONTAINER_ID" => "search-form-global",  // ID контейнера, по ширине которого будут выводиться результаты
            "INPUT_ID" => "title-search-input",  // ID строки ввода поискового запроса
            "NUM_CATEGORIES" => "1",  // Количество категорий поиска
            "ORDER" => "date",  // Сортировка результатов
            "PAGE" => "#SITE_DIR#search/",  // Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
            "SHOW_INPUT" => "Y",  // Показывать форму ввода поискового запроса
            "SHOW_OTHERS" => "N",  // Показывать категорию "прочее"
            "TOP_COUNT" => "5",  // Количество результатов в каждой категории
            "USE_LANGUAGE_GUESS" => "N",  // Включить автоопределение раскладки клавиатуры
            "COMPONENT_TEMPLATE" => "visual",
            "PRICE_CODE" => "",  // Тип цены
            "PRICE_VAT_INCLUDE" => "Y",  // Включать НДС в цену
            "PREVIEW_TRUNCATE_LEN" => "",  // Максимальная длина анонса для вывода
            "SHOW_PREVIEW" => "Y",  // Показать картинку
            "CONVERT_CURRENCY" => "N",  // Показывать цены в одной валюте
            "PREVIEW_WIDTH" => "75",  // Ширина картинки
            "PREVIEW_HEIGHT" => "75",  // Высота картинки
          ),
            false
          ); ?>
        </div>
      </div>
      <!--adding advertisment-->
      <div class="add-button-main col-lg-3 col-md-3 col-sm-3 hidden-xs">
        <p class="add-button-desktop">
          <a href="/account/add">Добавить товар</a>
        </p>
      </div>
      <!--account control-->
      <div class="account-control-main col-lg-2 col-md-2 hidden-sm hidden-xs">
        <div class="account-control-desktop">
          <? $APPLICATION->IncludeComponent(
            "bitrix:system.auth.form",
            "header.login.form",
            Array(
              "FORGOT_PASSWORD_URL" => "",
              "PROFILE_URL" => "/account/advertisement",
              "REGISTER_URL" => "/account/registration",
              "SHOW_ERRORS" => "N"
            )
          );
          //header account menu include
          if (!strpos($APPLICATION->GetCurPage(), 'confirm')) {
            $APPLICATION->IncludeComponent(
              "bitrix:main.include",
              "",
              Array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => "/includes/account_menu_header.php"
              )
            );
          }
          ?>
        </div>
      </div>

      <!--mobile button-->
      <div class="mobile-button-wrapper hidden-lg col-sm-1 col-xs-6">
        <div class="add-button-mobile">
          <a href="/account/add"></a>
        </div>

        <div class="search-icon-mobile"></div>

        <? $current_user = CUser::GetByID($USER->GetID()); ?>
        <? if (!empty($current_user->arResult)) : ?>
          <div class="account-control-mobile logged">
            <a href="/account/advertisement"></a>
          </div>
        <? else : ?>
          <div class="account-control-mobile anonymous">
            <a href="/account/login"></a>
          </div>
        <? endif ?>
      </div>
    </div>
    <div class="hidden-lg hidden-md hidden-sm search-global-mobile">
      <div id="search-form-global desktop" class="global-search">
        <? $APPLICATION->IncludeComponent("bitrix:search.title", "header.search.title", Array(
          "CATEGORY_0" => array(  // Ограничение области поиска
            0 => "iblock_catalog",
          ),
          "CATEGORY_0_TITLE" => "Объявления",  // Название категории
          "CATEGORY_0_iblock_catalog" => array(  // Искать в информационных блоках типа "iblock_catalog"
            0 => "all",
          ),
          "CHECK_DATES" => "N",  // Искать только в активных по дате документах
          "CONTAINER_ID" => "search-form-global",  // ID контейнера, по ширине которого будут выводиться результаты
          "INPUT_ID" => "title-search-input",  // ID строки ввода поискового запроса
          "NUM_CATEGORIES" => "1",  // Количество категорий поиска
          "ORDER" => "date",  // Сортировка результатов
          "PAGE" => "#SITE_DIR#search/",  // Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
          "SHOW_INPUT" => "Y",  // Показывать форму ввода поискового запроса
          "SHOW_OTHERS" => "N",  // Показывать категорию "прочее"
          "TOP_COUNT" => "5",  // Количество результатов в каждой категории
          "USE_LANGUAGE_GUESS" => "N",  // Включить автоопределение раскладки клавиатуры
          "COMPONENT_TEMPLATE" => "visual",
          "PRICE_CODE" => "",  // Тип цены
          "PRICE_VAT_INCLUDE" => "Y",  // Включать НДС в цену
          "PREVIEW_TRUNCATE_LEN" => "",  // Максимальная длина анонса для вывода
          "SHOW_PREVIEW" => "Y",  // Показать картинку
          "CONVERT_CURRENCY" => "N",  // Показывать цены в одной валюте
          "PREVIEW_WIDTH" => "75",  // Ширина картинки
          "PREVIEW_HEIGHT" => "75",  // Высота картинки
        ),
          false
        ); ?>
      </div>
    </div>
  </div>  <!-- header wrapper end-->
</header>

<?$APPLICATION->ShowViewContent('top_banner');?>

<div class="wrapper main-wrapper-container"> <!-- wrapper -->
  <div class="container"> <!-- container -->
  <? $page = $APPLICATION->GetCurPage(); ?>
  <div class="catalog-menu-mobile-wrapper">
    <div class="menu-close"><i class="fa fa-times" aria-hidden="true"></i></div>
    <div class="mobile-menu-header">
      <div class="modile-menu-logo">
        <a href="/">
          <img src="<?= SITE_TEMPLATE_PATH ?>/images/logo_icon.png">
        </a>
      </div>
      <div class="mobile-menu-close">
        <i class="fa fa-times" aria-hidden="true"></i>
      </div>
    </div>
    <? if (strpos($page, 'account') && !empty($current_user->arResult)) : ?>
      <? $APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
          "AREA_FILE_SHOW" => "file",
          "AREA_FILE_SUFFIX" => "inc",
          "EDIT_TEMPLATE" => "",
          "PATH" => "/includes/account_menu_mobile.php"
        )
      ); ?>
    <? endif; ?>
    <? $APPLICATION->IncludeComponent(
      "bitrix:menu",
      "main.menu.left",
      Array(
        "ALLOW_MULTI_SELECT" => "N",
        "CHILD_MENU_TYPE" => "main",
        "COMPONENT_TEMPLATE" => "catalog_vertical",
        "DELAY" => "N",
        "MAX_LEVEL" => "2",
        "MENU_CACHE_GET_VARS" => "",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_TYPE" => "N",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_THEME" => "site",
        "ROOT_MENU_TYPE" => "main",
        "USE_EXT" => "N",
        "PREFIX_MENU" => "mobile"
      )
    ); ?>
  </div>
  <? if ($page == '/') : ?>
    <div class="main-menu-tiles">
      <? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "main.menu", Array(
        "ADD_SECTIONS_CHAIN" => "Y",  // Включать раздел в цепочку навигации
        "CACHE_GROUPS" => "Y",  // Учитывать права доступа
        "CACHE_TIME" => "36000000",  // Время кеширования (сек.)
        "CACHE_TYPE" => "A",  // Тип кеширования
        "COUNT_ELEMENTS" => "Y",  // Показывать количество элементов в разделе
        "IBLOCK_ID" => "1",  // Инфоблок
        "IBLOCK_TYPE" => "catalog",  // Тип инфоблока
        "SECTION_CODE" => "",  // Код раздела
        "SECTION_FIELDS" => array(  // Поля разделов
          0 => "",
          1 => "",
        ),
        "SECTION_ID" => $_REQUEST["SECTION_ID"],  // ID раздела
        "SECTION_URL" => "",  // URL, ведущий на страницу с содержимым раздела
        "SECTION_USER_FIELDS" => array(  // Свойства разделов
          0 => "",
          1 => "",
        ),
        "SHOW_PARENT_NAME" => "Y",  // Показывать название раздела
        "TOP_DEPTH" => "2",  // Максимальная отображаемая глубина разделов
        "VIEW_MODE" => "LINE",  // Вид списка подразделов
      ),
        false
      ); ?>
    </div>
  <? endif; ?>
  <div class="content content-general"> <!-- content -->
    <!--Breadcrumbs and back button-->
    <?
    $current_page = explode('/', $page);
    if (!in_array($current_page[2], array('registration', 'login', '')) && !empty($current_page)):?>
    <div class="catalog-control-buttons">
      <div class="back-button hidden-lg hidden-md">
        <a href="<?= getPreviousPage() ?>">
          <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </a>
      </div>
      <?if (!strpos($APPLICATION->GetCurPage(), 'confirm')) :?>
        <div class="row">
          <div class="col-xs-9">
            <div class="breadcrumbs hidden-sm hidden-xs">
              <? $APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "custom.breadcrumbs",
                array(
                  "PATH" => "",
                  "SITE_ID" => "s1",
                  "START_FROM" => "0"
                )
              ); ?>
            </div>
          </div>
          <?$APPLICATION->ShowViewContent('show_filter_button');?>
        </div>
      <?endif;?>
    </div>
    <? endif;?>

	
						