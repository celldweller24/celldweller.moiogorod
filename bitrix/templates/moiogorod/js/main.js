$(document).ready(function () {
  /* BX slider */
  $('.slide-controls').bxSlider({
    slideWidth: 150,
    adaptiveHeight: true,
   /* minSlides: 1,
    maxSlides: 3,*/
    slideMargin: 0,
    moveSlides: 1,
    pager: false,
    nextSelector: '#slider-next',
    prevSelector: '#slider-prev',
    nextText: '<i class="fa fa-long-arrow-right" aria-hidden="true"></i>',
    prevText: '<i class="fa fa-long-arrow-left" aria-hidden="true"></i>'
  });

  $(".fancybox-detail").fancybox();

  if($("[name='REGISTER[PERSONAL_MOBILE]']").length){
      $("[name='REGISTER[PERSONAL_MOBILE]']").mask("+7(999)999-99-99");
  }

  if($(".add-ad-form [name='PROPERTY[2][0]']").length){
      $(".add-ad-form [name='PROPERTY[2][0]']").mask("+7(999)999-99-99");
  }

  $(".slide-controls img").click(function () {
    $(".slide-big-container img").attr("src", $(this).attr("src"));
    $(".slide-big-container a").attr("href", $(this).attr("origin-href"));

  });
  /* End BX slider */

  /*$('.category-menu-control').on('click', function() {
   if ($(this).attr('aria-expanded') == 'true'){
   $('.category-menu-control i').removeClass('fa-rotate-180');
   }
   else {
   $('.category-menu-control i').addClass('fa-rotate-180');
   $('.category-menu-control i').css('padding-bottom', '8px');
   }
   });*/

  // Filter popup and overlay layer
  $('.filter-button').on('click', function () {
    $('.catalog-filter-wrapper').addClass('filter-open');
    $('.overlay').addClass('overlay-visible');
  });

  $('.filter-close').on('click', function () {
    $('.catalog-filter-wrapper').removeClass('filter-open');
    $('.overlay').removeClass('overlay-visible');
  });

  $('.overlay').on('click', function () {
    $('.catalog-filter-wrapper').removeClass('filter-open mobile-menu-open');
    $('.overlay').removeClass('overlay-visible');
  });

  // Mobile menu popup and overlay layer
  $('.menu-button-responsive').on('click', function () {
    $('.catalog-menu-mobile-wrapper').addClass('mobile-menu-open');
    $('.overlay').addClass('overlay-visible');
  });

  $('.mobile-menu-close i').on('click', function () {
    $('.catalog-menu-mobile-wrapper').removeClass('mobile-menu-open');
    $('.overlay').removeClass('overlay-visible');
  });

  $('.overlay').on('click', function () {
    $('.catalog-menu-mobile-wrapper').removeClass('mobile-menu-open');
    $('.overlay').removeClass('overlay-visible');
  });

})