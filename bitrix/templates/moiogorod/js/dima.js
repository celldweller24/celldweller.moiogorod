$(document).ready(function () {
    $('#form_review .stars i').hover(
        function(){
            var star_num = $(this).attr('data-count');

            for(var i=1; i<=5; i++){
                $('#form_review .stars i.st-'+i).addClass('active');
                if(i == star_num)
                    break;
            }
            $(this).addClass('active');
        },
        function(){
            $('#form_review .stars i').removeClass('active');
        }
    );

    $('#form_review .stars i').click(function(){
        $('#form_review .stars i').removeClass('active-click');

        var star_num = $(this).data('count');

        $('#form_review input[name="stars"]').val(star_num);

        for(var i=1; i<=5; i++){
            $('#form_review .stars i.st-'+i).addClass('active-click');
            if(i == star_num)
                break;
        }
        $(this).addClass('active-click');
    });

    $('.reviews-list .review-item .answer-button').click(function(){
       var reviewText = $(this).parents('.review-item').find('p.review-text').text();
       var userName = $(this).parents('.review-item').find('p.title .user-name').text();
       var inputField = $('#form_review [name=review]');
       var message = '[quote]\n[b]'+userName+':[/b]\n'+reviewText+'\n[/quote]\n';

        inputField.addClass('with-quote');
       inputField.val(message);

       //form_review
        scrollToObj('#form_review');
    });
});

function scrollToObj(elemid, speed = 800) {
    var destination = $(elemid).offset().top;
    $('html, body').animate({ scrollTop: destination }, speed);
    //return true;
}