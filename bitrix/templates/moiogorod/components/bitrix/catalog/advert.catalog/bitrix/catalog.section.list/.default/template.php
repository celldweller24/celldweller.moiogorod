<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>
<? if (!empty($arResult['SECTIONS'])) : ?>
  <? $grid_array = getBootstrapGrid($arResult['SECTIONS']); ?>
  <div class="section-list-wrapper">
    <? foreach ($arResult['SECTIONS'] as &$arSection) :
      $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
      $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams); ?>

      <div id="<? echo $this->GetEditAreaId($arSection['ID']); ?>"
           class="section-item col-lg-<?= $grid_array['col_index'] ?> col-md-<?= $grid_array['col_index'] ?> col-xs-12">
        <a href="<? echo $arSection['SECTION_PAGE_URL']; ?>">
          <?= $arSection['NAME']; ?>
        </a>
      </div>
    <? endforeach ?>
  </div>
<? endif; ?>
