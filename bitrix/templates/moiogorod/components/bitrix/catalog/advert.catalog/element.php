<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);
?>
<div class="row">
    <div class="left-menu-element col-md-3 hidden-sm hidden-xs">
        <? $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "main.menu.left",
            [
                "ALLOW_MULTI_SELECT"    => "N",
                "CHILD_MENU_TYPE"       => "main",
                "COMPONENT_TEMPLATE"    => "catalog_vertical",
                "DELAY"                 => "N",
                "MAX_LEVEL"             => "2",
                "MENU_CACHE_GET_VARS"   => "",
                "MENU_CACHE_TIME"       => "3600",
                "MENU_CACHE_TYPE"       => "N",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_THEME"            => "site",
                "ROOT_MENU_TYPE"        => "main",
                "USE_EXT"               => "N",
                "PREFIX_MENU"           => "element"
            ]
        ); ?>
    </div>
    <div class="col-md-6 content-wrapper">
        <? $ElementID = $APPLICATION->IncludeComponent(
            "bitrix:catalog.element",
            "",
            [
                "IBLOCK_TYPE"                => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID"                  => $arParams["IBLOCK_ID"],
                "PROPERTY_CODE"              => $arParams["DETAIL_PROPERTY_CODE"],
                "META_KEYWORDS"              => $arParams["DETAIL_META_KEYWORDS"],
                "META_DESCRIPTION"           => $arParams["DETAIL_META_DESCRIPTION"],
                "BROWSER_TITLE"              => $arParams["DETAIL_BROWSER_TITLE"],
                "SET_CANONICAL_URL"          => $arParams["DETAIL_SET_CANONICAL_URL"],
                "BASKET_URL"                 => $arParams["BASKET_URL"],
                "ACTION_VARIABLE"            => $arParams["ACTION_VARIABLE"],
                "PRODUCT_ID_VARIABLE"        => $arParams["PRODUCT_ID_VARIABLE"],
                "SECTION_ID_VARIABLE"        => $arParams["SECTION_ID_VARIABLE"],
                "CHECK_SECTION_ID_VARIABLE"  => (isset($arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"]) ? $arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"] : ''),
                "PRODUCT_QUANTITY_VARIABLE"  => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                "PRODUCT_PROPS_VARIABLE"     => $arParams["PRODUCT_PROPS_VARIABLE"],
                //"CACHE_TYPE"                 => $arParams["CACHE_TYPE"],
                "CACHE_TYPE"                 => 'N',
                "CACHE_TIME"                 => $arParams["CACHE_TIME"],
                "CACHE_GROUPS"               => $arParams["CACHE_GROUPS"],
                "SET_TITLE"                  => $arParams["SET_TITLE"],
                "SET_LAST_MODIFIED"          => $arParams["SET_LAST_MODIFIED"],
                "MESSAGE_404"                => $arParams["MESSAGE_404"],
                "SET_STATUS_404"             => $arParams["SET_STATUS_404"],
                "SHOW_404"                   => $arParams["SHOW_404"],
                "FILE_404"                   => $arParams["FILE_404"],
                "PRICE_CODE"                 => $arParams["PRICE_CODE"],
                "USE_PRICE_COUNT"            => $arParams["USE_PRICE_COUNT"],
                "SHOW_PRICE_COUNT"           => $arParams["SHOW_PRICE_COUNT"],
                "PRICE_VAT_INCLUDE"          => $arParams["PRICE_VAT_INCLUDE"],
                "PRICE_VAT_SHOW_VALUE"       => $arParams["PRICE_VAT_SHOW_VALUE"],
                "USE_PRODUCT_QUANTITY"       => $arParams['USE_PRODUCT_QUANTITY'],
                "PRODUCT_PROPERTIES"         => $arParams["PRODUCT_PROPERTIES"],
                "ADD_PROPERTIES_TO_BASKET"   => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                "LINK_IBLOCK_TYPE"           => $arParams["LINK_IBLOCK_TYPE"],
                "LINK_IBLOCK_ID"             => $arParams["LINK_IBLOCK_ID"],
                "LINK_PROPERTY_SID"          => $arParams["LINK_PROPERTY_SID"],
                "LINK_ELEMENTS_URL"          => $arParams["LINK_ELEMENTS_URL"],

                "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                "OFFERS_FIELD_CODE"      => $arParams["DETAIL_OFFERS_FIELD_CODE"],
                "OFFERS_PROPERTY_CODE"   => $arParams["DETAIL_OFFERS_PROPERTY_CODE"],
                "OFFERS_SORT_FIELD"      => $arParams["OFFERS_SORT_FIELD"],
                "OFFERS_SORT_ORDER"      => $arParams["OFFERS_SORT_ORDER"],
                "OFFERS_SORT_FIELD2"     => $arParams["OFFERS_SORT_FIELD2"],
                "OFFERS_SORT_ORDER2"     => $arParams["OFFERS_SORT_ORDER2"],

                "ELEMENT_ID"               => $arResult["VARIABLES"]["ELEMENT_ID"],
                "ELEMENT_CODE"             => $arResult["VARIABLES"]["ELEMENT_CODE"],
                "SECTION_ID"               => $arResult["VARIABLES"]["SECTION_ID"],
                "SECTION_CODE"             => $arResult["VARIABLES"]["SECTION_CODE"],
                "SECTION_URL"              => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                "DETAIL_URL"               => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                'CONVERT_CURRENCY'         => $arParams['CONVERT_CURRENCY'],
                'CURRENCY_ID'              => $arParams['CURRENCY_ID'],
                'HIDE_NOT_AVAILABLE'       => $arParams["HIDE_NOT_AVAILABLE"],
                'USE_ELEMENT_COUNTER'      => $arParams['USE_ELEMENT_COUNTER'],
                'SHOW_DEACTIVATED'         => $arParams['SHOW_DEACTIVATED'],
                "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],

                'ADD_PICT_PROP'                => $arParams['ADD_PICT_PROP'],
                'LABEL_PROP'                   => $arParams['LABEL_PROP'],
                'OFFER_ADD_PICT_PROP'          => $arParams['OFFER_ADD_PICT_PROP'],
                'OFFER_TREE_PROPS'             => $arParams['OFFER_TREE_PROPS'],
                'PRODUCT_SUBSCRIPTION'         => $arParams['PRODUCT_SUBSCRIPTION'],
                'SHOW_DISCOUNT_PERCENT'        => $arParams['SHOW_DISCOUNT_PERCENT'],
                'SHOW_OLD_PRICE'               => $arParams['SHOW_OLD_PRICE'],
                'SHOW_MAX_QUANTITY'            => $arParams['DETAIL_SHOW_MAX_QUANTITY'],
                'MESS_BTN_BUY'                 => $arParams['MESS_BTN_BUY'],
                'MESS_BTN_ADD_TO_BASKET'       => $arParams['MESS_BTN_ADD_TO_BASKET'],
                'MESS_BTN_SUBSCRIBE'           => $arParams['MESS_BTN_SUBSCRIBE'],
                'MESS_BTN_COMPARE'             => $arParams['MESS_BTN_COMPARE'],
                'MESS_NOT_AVAILABLE'           => $arParams['MESS_NOT_AVAILABLE'],
                'USE_VOTE_RATING'              => $arParams['DETAIL_USE_VOTE_RATING'],
                'VOTE_DISPLAY_AS_RATING'       => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING']) ? $arParams['DETAIL_VOTE_DISPLAY_AS_RATING'] : ''),
                'USE_COMMENTS'                 => $arParams['DETAIL_USE_COMMENTS'],
                'BLOG_USE'                     => (isset($arParams['DETAIL_BLOG_USE']) ? $arParams['DETAIL_BLOG_USE'] : ''),
                'BLOG_URL'                     => (isset($arParams['DETAIL_BLOG_URL']) ? $arParams['DETAIL_BLOG_URL'] : ''),
                'BLOG_EMAIL_NOTIFY'            => (isset($arParams['DETAIL_BLOG_EMAIL_NOTIFY']) ? $arParams['DETAIL_BLOG_EMAIL_NOTIFY'] : ''),
                'VK_USE'                       => (isset($arParams['DETAIL_VK_USE']) ? $arParams['DETAIL_VK_USE'] : ''),
                'VK_API_ID'                    => (isset($arParams['DETAIL_VK_API_ID']) ? $arParams['DETAIL_VK_API_ID'] : 'API_ID'),
                'FB_USE'                       => (isset($arParams['DETAIL_FB_USE']) ? $arParams['DETAIL_FB_USE'] : ''),
                'FB_APP_ID'                    => (isset($arParams['DETAIL_FB_APP_ID']) ? $arParams['DETAIL_FB_APP_ID'] : ''),
                'BRAND_USE'                    => (isset($arParams['DETAIL_BRAND_USE']) ? $arParams['DETAIL_BRAND_USE'] : 'N'),
                'BRAND_PROP_CODE'              => (isset($arParams['DETAIL_BRAND_PROP_CODE']) ? $arParams['DETAIL_BRAND_PROP_CODE'] : ''),
                'DISPLAY_NAME'                 => (isset($arParams['DETAIL_DISPLAY_NAME']) ? $arParams['DETAIL_DISPLAY_NAME'] : ''),
                'ADD_DETAIL_TO_SLIDER'         => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : ''),
                'TEMPLATE_THEME'               => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                "ADD_SECTIONS_CHAIN"           => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
                "ADD_ELEMENT_CHAIN"            => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
                "DISPLAY_PREVIEW_TEXT_MODE"    => (isset($arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE']) ? $arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'] : ''),
                "DETAIL_PICTURE_MODE"          => (isset($arParams['DETAIL_DETAIL_PICTURE_MODE']) ? $arParams['DETAIL_DETAIL_PICTURE_MODE'] : ''),
                'ADD_TO_BASKET_ACTION'         => $basketAction,
                'SHOW_CLOSE_POPUP'             => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                'DISPLAY_COMPARE'              => (isset($arParams['USE_COMPARE']) ? $arParams['USE_COMPARE'] : ''),
                'COMPARE_PATH'                 => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
                'SHOW_BASIS_PRICE'             => (isset($arParams['DETAIL_SHOW_BASIS_PRICE']) ? $arParams['DETAIL_SHOW_BASIS_PRICE'] : 'Y'),
                'BACKGROUND_IMAGE'             => (isset($arParams['DETAIL_BACKGROUND_IMAGE']) ? $arParams['DETAIL_BACKGROUND_IMAGE'] : ''),
                'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
                'SET_VIEWED_IN_COMPONENT'      => (isset($arParams['DETAIL_SET_VIEWED_IN_COMPONENT']) ? $arParams['DETAIL_SET_VIEWED_IN_COMPONENT'] : ''),

                "USE_GIFTS_DETAIL"                => $arParams['USE_GIFTS_DETAIL'] ?: 'Y',
                "USE_GIFTS_MAIN_PR_SECTION_LIST"  => $arParams['USE_GIFTS_MAIN_PR_SECTION_LIST'] ?: 'Y',
                "GIFTS_SHOW_DISCOUNT_PERCENT"     => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
                "GIFTS_SHOW_OLD_PRICE"            => $arParams['GIFTS_SHOW_OLD_PRICE'],
                "GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
                "GIFTS_DETAIL_HIDE_BLOCK_TITLE"   => $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE'],
                "GIFTS_DETAIL_TEXT_LABEL_GIFT"    => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],
                "GIFTS_DETAIL_BLOCK_TITLE"        => $arParams["GIFTS_DETAIL_BLOCK_TITLE"],
                "GIFTS_SHOW_NAME"                 => $arParams['GIFTS_SHOW_NAME'],
                "GIFTS_SHOW_IMAGE"                => $arParams['GIFTS_SHOW_IMAGE'],
                "GIFTS_MESS_BTN_BUY"              => $arParams['GIFTS_MESS_BTN_BUY'],

                "GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
                "GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE"        => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'],
            ],
            $component
        ); ?>

        <? /* Comments */ ?>
        <div class="reviews-block">
            <p class="reviews-title">
                <span class="left">Комментарии</span>
                <? if (CUser::IsAuthorized()): ?>
                    <a href="#form_review" class="right">Добавить комментарий</a>
                <?endif?>
                <span class="clearfix"></span>
            </p>
            <div class="reviews-list">
                <?
                $res         = CIBlockElement::GetByID($arResult["VARIABLES"]["ELEMENT_ID"]);
                $ar_res      = $res->GetNext();
                $adCreatedBy = $ar_res['CREATED_BY'];
                ?>
                <? $GLOBALS['arrFilter_reviews'] = ["property_ITEM_ID" => $ElementID]; ?>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "reviews_list",
                    [
                        "ACTIVE_DATE_FORMAT"              => "d.m.Y \\\ H:m",
                        "ADD_SECTIONS_CHAIN"              => "N",
                        "AJAX_MODE"                       => "N",
                        "AJAX_OPTION_ADDITIONAL"          => "",
                        "AJAX_OPTION_HISTORY"             => "N",
                        "AJAX_OPTION_JUMP"                => "N",
                        "AJAX_OPTION_STYLE"               => "N",
                        "CACHE_FILTER"                    => "N",
                        "CACHE_GROUPS"                    => "Y",
                        "CACHE_TIME"                      => "36000000",
                        "CACHE_TYPE"                      => "A",
                        "CHECK_DATES"                     => "Y",
                        "DETAIL_URL"                      => "",
                        "DISPLAY_BOTTOM_PAGER"            => "Y",
                        "DISPLAY_DATE"                    => "N",
                        "DISPLAY_NAME"                    => "N",
                        "DISPLAY_PICTURE"                 => "N",
                        "DISPLAY_PREVIEW_TEXT"            => "N",
                        "DISPLAY_TOP_PAGER"               => "N",
                        "FIELD_CODE"                      => [
                            0 => "",
                            1 => "",
                        ],
                        "FILTER_NAME"                     => "arrFilter_reviews",
                        "HIDE_LINK_WHEN_NO_DETAIL"        => "N",
                        "IBLOCK_ID"                       => "5",
                        "IBLOCK_TYPE"                     => "catalog",
                        "INCLUDE_IBLOCK_INTO_CHAIN"       => "N",
                        "INCLUDE_SUBSECTIONS"             => "Y",
                        "MESSAGE_404"                     => "",
                        "NEWS_COUNT"                      => "1000",
                        "PAGER_BASE_LINK_ENABLE"          => "N",
                        "PAGER_DESC_NUMBERING"            => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL"                  => "N",
                        "PAGER_SHOW_ALWAYS"               => "N",
                        "PAGER_TEMPLATE"                  => ".default",
                        "PAGER_TITLE"                     => "Новости",
                        "PARENT_SECTION"                  => "",
                        "PARENT_SECTION_CODE"             => "",
                        "PREVIEW_TRUNCATE_LEN"            => "",
                        "PROPERTY_CODE"                   => [
                            0 => "TEXT_REVIEW",
                            1 => "RAITING",
                            2 => "ITEM_ID",
                            3 => "USER_ID",
                            4 => "",
                        ],
                        "SET_BROWSER_TITLE"               => "N",
                        "SET_LAST_MODIFIED"               => "N",
                        "SET_META_DESCRIPTION"            => "N",
                        "SET_META_KEYWORDS"               => "N",
                        "SET_STATUS_404"                  => "N",
                        "SET_TITLE"                       => "N",
                        "SHOW_404"                        => "N",
                        "SORT_BY1"                        => "ID",
                        "SORT_BY2"                        => "SORT",
                        "SORT_ORDER1"                     => "ASC",
                        "SORT_ORDER2"                     => "ASC",
                        "COMPONENT_TEMPLATE"              => "reviews_list",
                        "AD_CREATED_BY_ID"                => $adCreatedBy,
                    ],
                    false
                ); ?>
            </div>
            <? if (CUser::IsAuthorized()): ?>
                <form action="/includes/add_comment.php" method="post" id="form_review">
                    <div class="form-title">
                        <p class="zagl">Ваш комментарий</p>
                        <? if ($adCreatedBy != CUser::GetID()): ?>
                            <p class="stars">
                                <i class="fa fa-star st-1" data-count='1'></i>
                                <i class="fa fa-star st-2" data-count='2'></i>
                                <i class="fa fa-star st-3" data-count='3'></i>
                                <i class="fa fa-star st-4" data-count='4'></i>
                                <i class="fa fa-star st-5" data-count='5'></i>
                            </p>
                        <? endif ?>
                    </div>
                    <div class="form-body">
                        <p class="field">
                            <? if ($_GET['message_error']): ?>
                            <span class="error"><?= $_GET['message_error'] ?><? endif ?></span>
                            <textarea name="review" placeholder="Комментарий"></textarea>
                        </p>
                        <input type="hidden" name="item_id" value="<?= $ElementID ?>"/>
                        <input type="hidden" name="stars"/>
                        <input type="hidden" name="backurl" value="<?= $APPLICATION->GetCurPage() ?>"/>
                        <p class="button"><input class="btn" type="submit" value="Отправить"/></p>
                    </div>
                </form>
                <?if($_GET['message_error']):?>
                    <script>
                        scrollToObj('#form_review', 0);
                    </script>
                <?endif?>
            <? else: ?>
                <p>Авторизуйтесь для возможности оставлять комментарии</p>
            <? endif ?>
        </div>
    </div>
    <div class="right-sidebar col-md-3">
        <div class="raiting-informer">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                ".default",
                array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "AREA_FILE_RECURSIVE" => "Y",
                    "EDIT_TEMPLATE" => "standard.php",
                    "COMPONENT_TEMPLATE" => ".default",
                    "PATH" => "/includes/statistic.php"
                    // сюда передать параметром тип товара и обработать в файле в массиве $arParams
                ),
                false
            );?>
        </div>
        <div class="vip-items">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                ".default",
                array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "AREA_FILE_RECURSIVE" => "Y",
                    "EDIT_TEMPLATE" => "standard.php",
                    "COMPONENT_TEMPLATE" => ".default",
                    "PATH" => "/includes/vip_ads.php"
                    // сюда передать параметром тип товара и обработать в файле в массиве $arParams
                ),
                false
            );?>
        </div>
    </div>
</div>

<?$this->SetViewTarget('top_banner');?>
<?
$rsSections = CIBlockSection::GetList(array(),array('IBLOCK_ID' => 1, '=CODE' => $arResult['VARIABLES']['SECTION_CODE']));
if ($arSection = $rsSections->Fetch())
{
    $res = CIBlockSection::GetByID($arSection['ID']);
    if($ar_res = $res->GetNext()){
        $background = CFile::GetPath($ar_res['DETAIL_PICTURE']);
    }
}
?>
<?if($background):?>
    <div class="header-background" style="background-image: url(<?=$background?>);">

    </div>
<?else:?>
    <div class="header-background" style="padding: 50px 0"></div>
<?endif?>
<?$this->EndViewTarget();?>