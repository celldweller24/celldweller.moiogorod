<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

$my_fields = array(
  '10' => 6, // категория
  '20' => 'IBLOCK_SECTION', // раздел
  '30' => 'NAME', // заголовок объявления
  //'40' => 5, // тип товара
  '50' => 'PREVIEW_TEXT', // описание
  '60' => 2, // телефон
  '70' => 1, // Местоположение
  '80' => 7, // кол-во твоара
  '90' => 8, // Единица измерения
  '100' => 10, // цена
  '110' => 4, // Торг
  '120' => 3, // фото
);
$arResult['PROPERTY_LIST'] = $my_fields;
$arResult['PROPERTY_LIST_FULL'][6]['LIST_TYPE'] = 'C';

$arResult['PROPERTY_LIST_FULL'][4]['MULTIPLE'] = 'Y';

global $USER;
$user_id = $USER->GetID();
$rsUser = CUser::GetByID($user_id);
$arUser = $rsUser->Fetch();
$user_phone = $arUser['PERSONAL_PHONE'];

if (!empty($arResult["ERRORS"])):?>
  <? ShowError(implode("<br />", $arResult["ERRORS"])) ?>
<?endif;
if (strlen($arResult["MESSAGE"]) > 0):?>
  <? ShowNote($arResult["MESSAGE"]) ?>
<? endif ?>

  <script>
    $(document).ready(function () {
	    
		$('.bx-yandex-map').css('width', '100%');
      
      	//$("label.item-type radio").prop("checked").removeClass('btn-success').addClass('btn-default');
      
		$("label.item-type").click(function(){
			if($(this).find('radio').prop("checked")){
				
				$("label.item-type").removeClass('btn-success').addClass('btn-default');
				
				$(this).removeClass('btn-success').addClass('btn-default');
			}
			else{
				$("label.item-type").removeClass('btn-success').addClass('btn-default');
				
				$(this).removeClass('btn-default').addClass('btn-success');
			}
		});

        //$("input[name='PROPERTY[NAME][0]']").click(function(){
        /*$(":input[name='PROPERTY[5][0]']").bind("keyup change", function(e){
            var nameVal = $(this).val();
            var element = $(this);
            if(nameVal.length > 1){
                $.getJSON('/account/add/item_type.php', {findStr: nameVal}, function(data){
                    $('#hint_item_type').html('');
                    $.each(data, function(key, val){
                        $('#hint_item_type').append('<span onclick="insertname(\''+val.UF_ITEM_TYPE+'\')">'+val.UF_ITEM_TYPE+'</span>');
                    });
                });
            }
            else{
                $('#hint_item_type').html('');
            }
        });*/


		$('.add-ad-form form').submit(function(){
			
			var send = new Array(); // проверка заполненности полей (1 - готов / 0 - не готов)			
			
			$('.add-ad-form form .form-group.required .field > input[type="text"], .add-ad-form form .form-group.required .field > textarea').each(function(indx, element){
				var field_name = $(element).attr('name');
				var pattern;
				
				switch (field_name){
					/*case 'PROPERTY[NAME][0]': // заголовок объявления
						pattern = /^[ёЁА-я-0-9]{2,}$/;
						break;
					*/
					case 'PROPERTY[10][0]': // цена
						//pattern = /^[0-9.,]{1,}$/;
						//pattern = /^[\d]+,[\d]{2}$/;
						pattern = /^[ ]*[\d]+[.]?[\d]{0,2}[ ]*$/;
						break;
					default:
						pattern = /^.{3,}$/; // для остальных
				}
				
				if(!pattern.test($(element).val())){
					send.push(error_form($(element)));
				}
				else{
					send.push(good_form($(element)));
				}
			});
			
			if(send.some(search_null) == false){ // ищем 0 в массиве. Если нулей нету - отправляем форму
				return true;
			}
			else{
                var destination = $('.error-field').first().offset().top;
                $('html, body').animate({ scrollTop: destination-100 }, 1100);

				return false;
			}
		});
		
		$('.add-ad-form form input[name="PROPERTY[10][0]"]').change(function(){
            var re = /[,]/;
            var str = $(this).val();
            var newstr = str.replace(re, ".");
            $(this).val(newstr);
		});
		
		
    });
    
	function error_form(elem) { // функция неправильно заполненных полей
		elem.addClass('animated shake error-field').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){ $(this).removeClass('animated shake')});
		return 0;
	}
	function good_form(elem) { // функция правильно заполненных полей
		elem.removeClass('animated shake error-field');
		return 1;
	}
	function search_null(number) { // поиск 0 в массиве, чтоб узнать заполненность формы
		return number < 1;
	}
    /*function insertname(nameVal){
        $(":input[name='PROPERTY[5][0]']").val(nameVal);
    }*/
  </script>
  
<div class="add-ad-form">
  <form class="form-horizontal" name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post" enctype="multipart/form-data">
    <?= bitrix_sessid_post() ?>
    <? if ($arParams["MAX_FILE_SIZE"] > 0): ?>
      <input type="hidden" name="MAX_FILE_SIZE" value="<?= $arParams["MAX_FILE_SIZE"] ?>"/>
    <? endif ?>
    <? if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])): ?>
    
      <? foreach ($arResult["PROPERTY_LIST"] as $propertyID): ?>
      <div class="form-group <?=(in_array($propertyID, $arResult["PROPERTY_REQUIRED"]))?"required":""?>">
        <div class="col-sm-3 control-label field-name">
          <? if (intval($propertyID) > 0): ?>
            <?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?>
          <? else: ?>
            <?= !empty($arParams["CUSTOM_TITLE_" . $propertyID]) ? $arParams["CUSTOM_TITLE_" . $propertyID] : GetMessage("IBLOCK_FIELD_" . $propertyID) ?>
          <? endif ?>
          
          <? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>
            <span class="required">*</span>
          <? endif ?>
        </div>
        <?
        if (intval($propertyID) > 0) {
          if (
            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
            &&
            $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
          )
            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
          elseif (
            (
              $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
              ||
              $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
            )
            &&
            $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
          )
            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
        } elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
          $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

        if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y") {
          $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
          $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
        } else {
          $inputNum = 1;
        }

        if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
          $INPUT_TYPE = "USER_TYPE";
        else
          $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

        ?>
        <div class="col-sm-5 field"><?
          switch ($INPUT_TYPE):
            case "USER_TYPE":
              for ($i = 0; $i < $inputNum; $i++) {
                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                  $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
                  $description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
                } elseif ($i == 0) {
                  $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                  $description = "";
                } else {
                  $value = "";
                  $description = "";
                }
                echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
                  array(
                    $arResult["PROPERTY_LIST_FULL"][$propertyID],
                    array(
                      "VALUE" => $value,
                      "DESCRIPTION" => $description,
                    ),
                    array(
                      "VALUE" => "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]",
                      "DESCRIPTION" => "PROPERTY[" . $propertyID . "][" . $i . "][DESCRIPTION]",
                      "FORM_NAME" => "iblock_add",
                    ),
                  ));
                ?><?
              }
              break;
            case "TAGS":
              $APPLICATION->IncludeComponent(
                "bitrix:search.tags.input",
                "",
                array(
                  "VALUE" => $arResult["ELEMENT"][$propertyID],
                  "NAME" => "PROPERTY[" . $propertyID . "][0]",
                  "TEXT" => 'size="' . $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] . '"',
                ), null, array("HIDE_ICONS" => "Y")
              );
              break;
            case "HTML":
              $LHE = new CHTMLEditor;
              $LHE->Show(array(
                'name' => "PROPERTY[" . $propertyID . "][0]",
                'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[" . $propertyID . "][0]"),
                'inputName' => "PROPERTY[" . $propertyID . "][0]",
                'content' => $arResult["ELEMENT"][$propertyID],
                'width' => '100%',
                'minBodyWidth' => 350,
                'normalBodyWidth' => 555,
                'height' => '200',
                'bAllowPhp' => false,
                'limitPhpAccess' => false,
                'autoResize' => true,
                'autoResizeOffset' => 40,
                'useFileDialogs' => false,
                'saveOnBlur' => true,
                'showTaskbars' => false,
                'showNodeNavi' => false,
                'askBeforeUnloadPage' => true,
                'bbCode' => false,
                'siteId' => SITE_ID,
                'controlsMap' => array(
                  array('id' => 'Bold', 'compact' => true, 'sort' => 80),
                  array('id' => 'Italic', 'compact' => true, 'sort' => 90),
                  array('id' => 'Underline', 'compact' => true, 'sort' => 100),
                  array('id' => 'Strikeout', 'compact' => true, 'sort' => 110),
                  array('id' => 'RemoveFormat', 'compact' => true, 'sort' => 120),
                  array('id' => 'Color', 'compact' => true, 'sort' => 130),
                  array('id' => 'FontSelector', 'compact' => false, 'sort' => 135),
                  array('id' => 'FontSize', 'compact' => false, 'sort' => 140),
                  array('separator' => true, 'compact' => false, 'sort' => 145),
                  array('id' => 'OrderedList', 'compact' => true, 'sort' => 150),
                  array('id' => 'UnorderedList', 'compact' => true, 'sort' => 160),
                  array('id' => 'AlignList', 'compact' => false, 'sort' => 190),
                  array('separator' => true, 'compact' => false, 'sort' => 200),
                  array('id' => 'InsertLink', 'compact' => true, 'sort' => 210),
                  array('id' => 'InsertImage', 'compact' => false, 'sort' => 220),
                  array('id' => 'InsertVideo', 'compact' => true, 'sort' => 230),
                  array('id' => 'InsertTable', 'compact' => false, 'sort' => 250),
                  array('separator' => true, 'compact' => false, 'sort' => 290),
                  array('id' => 'Fullscreen', 'compact' => false, 'sort' => 310),
                  array('id' => 'More', 'compact' => true, 'sort' => 400)
                ),
              ));
              break;
            case "T":
              for ($i = 0; $i < $inputNum; $i++) {

                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                  $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                } elseif ($i == 0) {
                  $value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                } else {
                  $value = "";
                }
                ?>
                <textarea class="form-control" name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"><?= $value ?></textarea>
                <?
              }
              break;

            case "S":
            case "N":
              for ($i = 0; $i < $inputNum; $i++) {
                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                  $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                } elseif ($i == 0) {
                  $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

                } else {
                  $value = "";
                }
                ?>
                  <?
                  $placeholder = "";
                  switch ($propertyID){
                      /*case '5':
                          $placeholder = "Например, Свинина, Мед, Грибы, Саженцы, Трактор";
                          break;*/
                      case '2':
                          if($value == ""){
                              $value = $user_phone;
                          }
                      default:
                          $placeholder = '';
                  }

                  ?>
                <input placeholder="<?=$placeholder?>" class="form-control" type="text" name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]" value="<?= $value ?>"/>

                  <?/*if($propertyID == 5):?>
                    <div id="hint_item_type"></div>
                  <?endif;*/?>

                <?
                /*if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"):?><?
                  $APPLICATION->IncludeComponent(
                    'bitrix:main.calendar',
                    '',
                    array(
                      'FORM_NAME' => 'iblock_add',
                      'INPUT_NAME' => "PROPERTY[" . $propertyID . "][" . $i . "]",
                      'INPUT_VALUE' => $value,
                    ),
                    null,
                    array('HIDE_ICONS' => 'Y')
                  );
                  ?><br/>
                  <small><?= GetMessage("IBLOCK_FORM_DATE_FORMAT") ?><?= FORMAT_DATETIME ?></small><?
                endif;*/
              }
              break;

            case "F":
              for ($i = 0; $i < $inputNum; $i++) {
                $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                ?>
                <input type="hidden"
                       name="PROPERTY[<?= $propertyID ?>][<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>]"
                       value="<?= $value ?>"/>
                <input type="file" size="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] ?>"
                       name="PROPERTY_FILE_<?= $propertyID ?>_<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>"/>
                <br/>
                <?

                if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value])) {
                  ?>
                  <input type="checkbox"
                         name="DELETE_FILE[<?= $propertyID ?>][<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>]"
                         id="file_delete_<?= $propertyID ?>_<?= $i ?>" value="Y"/><label
                    for="file_delete_<?= $propertyID ?>_<?= $i ?>"><?= GetMessage("IBLOCK_FORM_FILE_DELETE") ?></label>
                  <br/>
                  <?

                  if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"]) {
                    ?>
                    <img src="<?= $arResult["ELEMENT_FILES"][$value]["SRC"] ?>"
                         height="<?= $arResult["ELEMENT_FILES"][$value]["HEIGHT"] ?>"
                         width="<?= $arResult["ELEMENT_FILES"][$value]["WIDTH"] ?>" border="0"/><br/>
                    <?
                  } else {
                    ?>
                    <?= GetMessage("IBLOCK_FORM_FILE_NAME") ?>: <?= $arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"] ?>
                    <br/>
                    <?= GetMessage("IBLOCK_FORM_FILE_SIZE") ?>: <?= $arResult["ELEMENT_FILES"][$value]["FILE_SIZE"] ?> b
                    <br/>
                    [<a
                      href="<?= $arResult["ELEMENT_FILES"][$value]["SRC"] ?>"><?= GetMessage("IBLOCK_FORM_FILE_DOWNLOAD") ?></a>]
                    <br/>
                    <?
                  }
                }
              }

              break;
            case "L":

              if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C") {
                $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
              }
              else
                $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

              switch ($type):
                case "checkbox":
                  foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum) {
                    $checked = false;
                    if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                      if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID])) {
                        foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum) {
                          if ($arElEnum["VALUE"] == $key) {
                            $checked = true;
                            break;
                          }
                        }
                      }
                    } else {
                      if ($arEnum["DEF"] == "Y") $checked = true;
                    }

                    ?>
                    <div class="checkbox">
	                    <label>
		                    <input type="<?=$type?>" name="PROPERTY[<?= $propertyID ?>]<?= $type == "checkbox" ? "[" . $key . "]" : "" ?>"
		                             value="<?= $key ?>"
		                             id="property_<?= $key ?>"<?= $checked ? " checked=\"checked\"" : "" ?> />
		                      <?= $arEnum["VALUE"] ?>
	                    </label>
                    </div>
                    <?
                  }
                  break;
                case "radio":

				$i = 0;
                  foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum) {
                   
                    $checked = false;
                    if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                      if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID])) {
                        foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum) {
                          if ($arElEnum["VALUE"] == $key) {
                            $checked = true;
                            break;
                          }
                        }
                      }
                    } else {
                      if ($arEnum["DEF"] == "Y") $checked = true;
                    }

                    ?>
                    <label class="btn <?=($i==0)?"btn-success":"btn-default"?> left item-type">
                    <input type="<?=$type?>" name="PROPERTY[<?= $propertyID ?>]<?= $type == "checkbox" ? "[" . $key . "]" : "" ?>"
                           value="<?= $key ?>"
                           id="property_<?= $key ?>"<?= $checked ? " checked=\"checked\"" : "" ?> />
                      <?= $arEnum["VALUE"] ?></label>
                    <?
	                $i++;
                  }
                  
                  break;

                case "dropdown":
                case "multiselect":
                  ?>
                  	<?if($propertyID == 'IBLOCK_SECTION'):?>
	                  <select class="selectpicker" data-live-search="true" name="PROPERTY[<?= $propertyID ?>]<?= $type == "multiselect" ? "[]" : "" ?>" data-none-results-text="Раздел не найден">
		            <?else:?>
			            <select class="selectpicker" name="PROPERTY[<?= $propertyID ?>]<?= $type == "multiselect" ? "[]" : "" ?>">
		            <?endif;?>
                    <!--<option disabled='disabled' selected="selected" value=""><? echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA") ?></option>-->
                    <?
                    if (intval($propertyID) > 0)
                      $sKey = "ELEMENT_PROPERTIES";
                    else $sKey = "ELEMENT";
                    
                    $bufDots = 1;
                    $bold = true;
                    //debug($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"]);
                    foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum) {
                      $checked = false;
                      if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                        foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
                          if ($key == $arElEnum["VALUE"]) {
                            $checked = true;
                            break;
                          }
                        }
                      } else {
                        if ($arEnum["DEF"] == "Y") $checked = true;
                      }

                        /*$bold = false;
                        $pos_dots = strripos($arEnum["VALUE"], ' .  . ');
                        if ($pos_dots === false){
                          $bold = true;
                        }

                      $option_name = trim($arEnum["VALUE"]);
                      $option_name = str_replace(".  . ", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $option_name);
                      $option_name = str_replace(". ", "", $option_name);
                      ?><option <?=($bold)?"style='font-weight: bold'":""?> value="<?= $key ?>" <?= $checked ? " selected=\"selected\"" : "" ?>><?=$option_name?></option><?*/

                        $option_name = $arEnum["VALUE"];

                        preg_match('/([ .]+).+/', $option_name, $matches);

                        if($matches[1]){
                            $dots = $matches[1];
                            $dots = str_replace(' ', '', $dots);

                            $dotsCount = mb_strlen($dots);

                            /*if($bufDots != $dotsCount && $bufDots > $dotsCount){
                                $needBorderBottom = true;
                            }

                            if($bufDots != $dotsCount && $bufDots < $dotsCount){
                                $needBorderTop = true;
                            }*/
                            if($dotsCount == 1){
                                $bold = true;
                            }

                            $bufDots = $dotsCount;

                        }

                        ?><option
                            <?=($bold)?"style='font-weight: bold'":"'"?>
                            value="<?= $key ?>"
                            <?= $checked ? " selected=\"selected\"" : "" ?>>
                                <?=$option_name?>
                        </option><?

                        $bold = false;
                    }
                    ?>
                  </select>
                  <?
                  break;
              endswitch;
              break;
          endswitch; ?>
          </div>
        </div>
        <? endforeach; ?>
      <? endif ?>
      
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-5">
				<input class="btn btn-success" type="submit" name="iblock_submit" value="<?= GetMessage("IBLOCK_FORM_SUBMIT") ?>"/>
				<?/* if (strlen($arParams["LIST_URL"]) > 0): ?>
				<input type="submit" name="iblock_apply" value="<?= GetMessage("IBLOCK_FORM_APPLY") ?>"/>
				<input
				type="button"
				name="iblock_cancel"
				value="<? echo GetMessage('IBLOCK_FORM_CANCEL'); ?>"
				onclick="location.href='<? echo CUtil::JSEscape($arParams["LIST_URL"]) ?>';"
				>
				<? endif */?>
			</div>
		</div>
  </form>
</div>