<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>
<div class="account-elements col-lg-6 col-md-6 col-sm-9 col-xs-12">
  <? if ($arResult["NO_USER"] == "N"): ?>
    <h1>Мои объявления</h1>
    <? if (count($arResult["ELEMENTS"]) > 0): ?>
      <? foreach ($arResult["ELEMENTS"] as $arElement): ?>
        <?
        // Get custom properties of current item.
        $currentSelect = array(
          "ID",
          "ACTIVE",
          "PROPERTY_LAST_UPDATE",
          "PROPERTY_ON_MODERATE",
          "PROPERTY_DELETED_BY_USER",
          "PROPERTY_ITEM_USER",
          "PROPERTY_ADDITIONAL_PHOTO"
        );
        $currentFilter = array("IBLOCK_ID" => 1, "ID" => $arElement["ID"]);
        $currentResult = CIBlockElement::GetList(array(), $currentFilter, false, false, $currentSelect);
        $currentItemProperty = $currentResult->GetNextElement()->fields;

        if (!empty($_GET['deleted_by_user'])) {
          if ($_GET['deleted_by_user'] == $arElement["ID"]) {
            $element = new CIBlockElement;
            $itemProperties = getAllElementProperty($arElement["ID"]);
            $itemProperties["DELETED_BY_USER"] = "13";
            $element->Update($arElement["ID"], array("ACTIVE" => "N"));
            $element->SetPropertyValuesEx($arElement["ID"], 1, $itemProperties);
          }
        }
        ?>
        <?if ($currentItemProperty["PROPERTY_ITEM_USER_VALUE"] == CUser::GetID()
          && !$currentItemProperty["PROPERTY_DELETED_BY_USER_ENUM_ID"]) :?>
          <?if ($_GET['deleted_by_user'] != $arElement["ID"]) :?>
            <div class="account-element-wrapper<?= ($currentItemProperty["ACTIVE"] == "Y") ? ' active' : ' deactive' ?>">
            <!--<div class="account-element-wrapper active">-->
              <div class="element-head row">
                <?/*if ($currentItemProperty["PROPERTY_ON_MODERATE_VALUE"]) :*/?><!--
                  <p class="ismoderate-message col-xs-12">Ваше объявление находится на модерации. Очень скоро он станет доступно</p>
                --><?/*else :*/?>
                  <p class="refresh-duration col-xs-6">Обновлено <?= $currentItemProperty["PROPERTY_LAST_UPDATE_VALUE"] ?></p>
                  <p class="pay-in col-xs-6">
                    <span class="pay-in-icon"></span>
                    <span class="pay-in-text">Поднять объявление</span>
                  </p>
                <?//endif;?>
              </div>
              <div class="account-element-body row">
                <?
                $arSelect = array("ID", "DETAIL_PAGE_URL");
                $arFilter = array("IBLOCK_ID" => 1, "ID" => $arElement["ID"]);
                $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
                $detailPageElement = $res->GetNextElement()->fields;
                ?>
                <p class="account-element-image col-xs-4">
                  <?$account_image = CFile::ResizeImageGet($currentItemProperty['PROPERTY_ADDITIONAL_PHOTO_VALUE'][0],
                    array('width' => '150', 'height' => '120'), BX_RESIZE_IMAGE_EXACT, true, array(), false, "80")?>
                  <a href="<?=$detailPageElement['DETAIL_PAGE_URL'] . $detailPageElement['ID']?>/">
                    <img src="<?= (!empty($account_image)) ? $account_image['src'] : SITE_TEMPLATE_PATH . '/images/no_image_photo.png' ?>">
                  </a>
                </p>
                <p class="account-element-title col-xs-5">
                  <?/*if (!$currentItemProperty["PROPERTY_ON_MODERATE_VALUE"]) :*/?>
                    <a href="<?=$detailPageElement['DETAIL_PAGE_URL'] . $detailPageElement['ID']?>/">
                      <?= $arElement["NAME"] ?>
                    </a>
                  <?/*else :*/?><!--
                    <span><?/*= $arElement["NAME"] */?></span>
                  --><?/*endif*/?>
                </p>
                <? $properties = CIBlockElement::GetProperty(1, $arElement['ID'], array(), array()); ?>
                <? foreach ($properties->arResult as $property) :?>
                  <? if ($property['CODE'] == 'ITEM_COST') :?>
                    <p class="account-element-price col-xs-3"><?=CurrencyFormat($property['VALUE'], 'RUB')?></p>
                  <?endif;?>
                <?endforeach;?>
              </div>
              <div class="advert-element-control row">
                <div class="col-lg-3 col-md-3 hidden-xs"></div>
                <p class="account-element-refresh col-lg-3 col-md-3 col-xs-4">
                  <?/*if (!$currentItemProperty["PROPERTY_ON_MODERATE_VALUE"]) :*/?>
                    <a href="?action=refresh&id=<?= $arElement['ID'] ?>" class="refresh-title">
                      <i class="fa fa-refresh" aria-hidden="true"></i>
                    </a>
                    <a href="?action=refresh&id=<?= $arElement['ID'] ?>">Обновить</a>
                  <?/*endif;*/?>
                </p>

                <? if ($arResult["CAN_EDIT"] == "Y"): ?>
                  <? if ($arElement["CAN_EDIT"] == "Y"): ?>
                    <p class="account-element-edit col-lg-3 col-md-3 col-xs-4">
                      <a href="<?= $arParams["EDIT_URL"] ?>?edit=Y&amp;CODE=<?= $arElement["ID"] ?>" class="edit-title">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                      </a>
                      <a href="<?= $arParams["EDIT_URL"] ?>?edit=Y&amp;CODE=<?= $arElement["ID"] ?>">
                        <?= (GetMessage("IBLOCK_ADD_LIST_EDIT")) ? GetMessage("IBLOCK_ADD_LIST_EDIT") : '' ?>
                      </a>
                    </p>
                  <? endif; ?>
                <? endif ?>
                <p class="account-element-delete col-lg-3 col-md-3 col-xs-4">
                  <a href="<?="?deleted_by_user=" . $arElement["ID"]?>" class="delete-title">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                  </a>
                  <a href="<?="?deleted_by_user=" . $arElement["ID"]?>">
                    <?= GetMessage("IBLOCK_ADD_LIST_DELETE") ?>
                  </a>
                </p>
                <?/* if ($arResult["CAN_DELETE"] == "Y"): */?><!--
                  <?/* if ($arElement["CAN_DELETE"] == "Y"): */?>
                    <p class="account-element-delete col-lg-3 col-md-3 col-xs-4">
                      <a href="?delete=Y&amp;CODE=<?/*= $arElement["ID"] */?>&amp;<?/*= bitrix_sessid_get() */?>"
                         class="delete-title"
                         onClick="return confirm('<?/* echo CUtil::JSEscape(str_replace("#ELEMENT_NAME#", $arElement["NAME"], GetMessage("IBLOCK_ADD_LIST_DELETE_CONFIRM"))) */?>')">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                      </a>
                      <a href="?delete=Y&amp;CODE=<?/*= $arElement["ID"] */?>&amp;<?/*= bitrix_sessid_get() */?>"
                         onClick="return confirm('<?/* echo CUtil::JSEscape(str_replace("#ELEMENT_NAME#", $arElement["NAME"], GetMessage("IBLOCK_ADD_LIST_DELETE_CONFIRM"))) */?>')">
                        <?/*= GetMessage("IBLOCK_ADD_LIST_DELETE") */?>
                      </a>
                    </p>
                  <?/* endif */?>
                --><?/* endif */?>
                <!--<p class="advert-visibility col-lg-6 col-md-3 col-xs-12">
                  <?/* if ($arElement['ACTIVE'] == 'Y') : */?>
                    <i class="fa fa-eye-slash" aria-hidden="true"></i>
                    <a href="?action=disable&amp;id=<?/*= $arElement['ID'] */?>">Отключить</a>
                  <?/* else : */?>
                    <i class="fa fa-eye" aria-hidden="true"></i>
                    <a href="?action=enable&amp;id=<?/*= $arElement['ID'] */?>">Включить</a>
                  <?/* endif; */?>
                </p>-->
              </div>
            </div>
          <?endif;?>
        <?endif;?>
      <? endforeach ?>
    <? else: ?>
      <p class="account-ad-empty"><?= GetMessage("IBLOCK_ADD_LIST_EMPTY") ?></p>
    <? endif ?>
  <? endif ?>
  <p class="account-add-new">
    <? if ($arParams["MAX_USER_ENTRIES"] > 0 && $arResult["ELEMENTS_COUNT"] < $arParams["MAX_USER_ENTRIES"]): ?>
      <a href="<?= $arParams["EDIT_URL"] ?>?edit=Y">Добавить товар</a>
    <? else: ?>
      <?= GetMessage("IBLOCK_LIST_CANT_ADD_MORE") ?>
    <? endif ?>
  </p>
</div>
<? if (strlen($arResult["NAV_STRING"]) > 0): ?><?= $arResult["NAV_STRING"] ?><? endif ?>
  