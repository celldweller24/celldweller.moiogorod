<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
checkAnonymRedirect();
$curentUser = CUser::GetByID(CUser::GetID())->Fetch();
if ($curentUser && $curentUser['CONFIRM_CODE_SUCCESS']) {
	LocalRedirect("/account/advertisement/");
}
$query = "SELECT CONFIRM_CODE_REGISTER FROM b_user WHERE ID=" . $curentUser['ID'];
$confirmCode = $DB->Query($query)->Fetch();

if (!empty($_POST['CONFIRM_CODE'])) {
	if ($confirmCode['CONFIRM_CODE_REGISTER'] == $_POST['CONFIRM_CODE']) {
		$query = "UPDATE b_user SET CONFIRM_CODE_SUCCESS=1 WHERE ID=" . $curentUser['ID'];
		$DB->Query($query);
		LocalRedirect("/account/advertisement/");
	}
}

// Resending confirmation code
if ($_POST['resend_confirm_code'] == 'resend') {
	$userPhone = preg_replace('(\D)', '', $curentUser['PERSONAL_MOBILE']);
  $confirmCode = mt_rand(1000, 9000);
  $message = ' Код подтверждения: ' . $confirmCode;

  $ch = curl_init("https://sms.ru/sms/send");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
    "api_id" => "89C332F2-1148-629D-7ADF-9CAB10D40D46",
    "to" => $userPhone,
    "msg" => $message,
    "json" => 1
  )));
  $response = curl_exec($ch);
  curl_close($ch);

  $json = json_decode($response);
  if ($json) {
    if ($json->status == "OK" && !isset($json->sms->_empty_)) {
      $query = "UPDATE b_user SET CONFIRM_CODE_REGISTER=" . $confirmCode . " WHERE ID=" . $curentUser['ID'];
      $DB->Query($query);
    }
  }
}

$APPLICATION->SetTitle("Подтверждение регистрации");?>

<div class="login-form-wrapper confirmation col-lg-6 col-md-6 col-sm-10 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-1">
	<? 
	if (!empty($_POST['CONFIRM_CODE'])) {
		if ($confirmCode['CONFIRM_CODE_REGISTER'] != $_POST['CONFIRM_CODE']) {
			ShowError('Неверный код подтверждения');
		}
	}
	?>
	<h2>Подтверждение регистрации</h2>
	<form name="system_auth_form" method="post">
		<div class="auth-form">
			<p class="pre-confirm-message">На ваш номер телефона была отправлена смс с кодом подтверждения</p>
			<p class="user-login-field">
				<input type="password" name="CONFIRM_CODE" class="form-control" value=""
				placeholder="Введите код подтверждения"/>
			</p>
			<div class="help-login">
				<div class="confirm-resend col-sm-12 col-xs-12">
					<p>
						<button type="submit" name="resend_confirm_code" value="resend">Выслать код подтверждения повторно?</button>
					</p>
				</div>
			</div>
			<p class="button-login">
				<input type="submit" name="Login" class="enter-button confirm" value="Подтвердить"/>
			</p>
			<p class="or-line">или</p>
			<div>
				<p class="register-button">
					<a href="/account/registration/" rel="nofollow"><?= GetMessage("AUTH_REGISTER") ?></a>
				</p>
			</div>
		</div>
	</form>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>