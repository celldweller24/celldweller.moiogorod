<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
checkAnonymRedirect();
$APPLICATION->SetTitle("Добавление нового объявления");
?><?$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.form", 
	"add_new_ad", 
	array(
		"COMPONENT_TEMPLATE" => "add_new_ad",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "Раздел",
		"CUSTOM_TITLE_NAME" => "Наименование товара",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "Подробно опишите товар или услугу",
		"CUSTOM_TITLE_TAGS" => "",
		"DEFAULT_INPUT_SIZE" => "",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"ELEMENT_ASSOC" => "PROPERTY_ID",
		"ELEMENT_ASSOC_PROPERTY" => "9",
		"GROUPS" => array(
			0 => "3",
			1 => "4",
		),
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "catalog",
		"LEVEL_LAST" => "Y",
		"LIST_URL" => "/account/",
		"MAX_FILE_SIZE" => "0",
		"MAX_LEVELS" => "100000",
		"MAX_USER_ENTRIES" => "100000",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"PROPERTY_CODES" => array(
			0 => "1",
			1 => "2",
			2 => "3",
			3 => "4",
			4 => "6",
			5 => "7",
			6 => "8",
			7 => "10",
			8 => "24",
			9 => "NAME",
			10 => "IBLOCK_SECTION",
			11 => "PREVIEW_TEXT",
			12 => "DETAIL_PICTURE",
		),
		"PROPERTY_CODES_REQUIRED" => array(
			0 => "2",
			1 => "6",
			2 => "10",
			3 => "NAME",
			4 => "IBLOCK_SECTION",
			5 => "PREVIEW_TEXT",
		),
		"RESIZE_IMAGES" => "Y",
		"SEF_MODE" => "N",
		"STATUS" => "ANY",
		"STATUS_NEW" => "N",
		"USER_MESSAGE_ADD" => "Объявление успешно добавлено",
		"USER_MESSAGE_EDIT" => "",
		"USE_CAPTCHA" => "N"
	),
	false
);?>
 <!--<form>
	<p class="field">
 <label class="btn btn-success">
		Продам <input name="ad_type" type="checkbox"> </label> <label class="btn btn-success">
		Куплю <input name="ad_type" type="checkbox"> </label>
	</p>
	<p class="field">
 <input name="" placeholder="Заголовок объвяления" type="text">
	</p>
</form>-->
<br/>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
