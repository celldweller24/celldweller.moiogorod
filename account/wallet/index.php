<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
checkAnonymRedirect();
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$APPLICATION->SetTitle("Мой кошелек");
?>
<?
/* Get user parameters*/
global $USER;
$curentUser = CUser::GetByID($USER->GetID())->Fetch();
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/account_menu.php"
	)
);?>
<div class="account-balance-wrapper col-lg-6 col-md-6 col-sm-9 col-xs-12">
  <h1>Мой кошелек</h1>
  <div class="curren-balance row">
  	<p class="balance-title col-lg-4 col-md-4 col-sm-5 col-xs-7">Баланс</p>
  	<!-- <p class="balance-amount"><?/*=CurrencyFormat($curentUser['UF_BALANCE'], 'RUB')*/?></p> -->
  	<p class="balance-amount col-lg-8 col-md-8 col-sm-7 col-xs-5"><?=($curentUser['UF_BALANCE']) ? $curentUser['UF_BALANCE'] : "0"?> руб</p>
  </div>
  <div class="add-balance">
  	<p class="adding-balance-input col-lg-8 col-md-8 col-sm-12 col-xs-12">
  	  <span>Пополнить баланс на сумму</span>
  	  <input type="text" value="">
  	 </p>
  	<p class="add-balance-button col-lg-4 col-md-4 col-sm-12 col-xs-12">
  		<a href="">Оплатить</a>
  	</p>
  </div>
</div>

<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
    "AREA_FILE_SHOW" => "file",
    "AREA_FILE_SUFFIX" => "inc",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/includes/banner.php"
  )
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>