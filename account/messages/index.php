<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
checkAnonymRedirect();

CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;
use Bitrix\Main\Type\DateTime;

const MY_HL_BLOCK_ID = 2;
$entity_data_class = GetEntityDataClass(MY_HL_BLOCK_ID);

$rsData = $entity_data_class::getList(array(
    'select' => array('*'),
    'order' => array('ID'=>'ASC'),
    'filter' => array('=UF_USER_ID_TO' => CUser::GetID()),
    'group' => array('UF_USER_ID_FROM')
));	

$arItems = array();
while($el = $rsData->fetch()){
    $arItems[] = $el;
}

foreach ($arItems as $key => $arItem) {
	foreach ($arItems as $keyEnclosed => $arItemEnclosed) {
		if ($arItem['UF_USER_ID_FROM'] == $arItemEnclosed['UF_USER_ID_FROM'] && $key != $keyEnclosed) {
			unset($arItems[$key]);
		}
	}
}

/*$data = array(
            "UF_USER_ID_FROM" => 3,
            "UF_USER_ID_TO" => 4,
            "UF_PRODUCT_ID" => 26,
            'UF_TYPE' => 'user',
            'UF_TITLE' => 'Title_34',
            'UF_BODY' => 'ололол',
            'UF_IS_READ' => false,
            'UF_IS_DELETED_TO' => false,
            'UF_IS_DELETED_FROM' => false,
            'UF_TIMESTAMP' => new DateTime()
        );
$result = $entity_data_class::add($data);*/



$APPLICATION->SetTitle("Мои сообщения");
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/account_menu.php"
	)
);?>

<div class="messages-wrapper col-lg-6 col-md-6 col-sm-9 col-xs-12">
	<h1>Мои сообщения</h1>
	<ul class="messages-list">
		<?if (!empty($arItem)) :?>
			<?$deletedMessages = []?>
			<?foreach($arItems as $arItem) :?>
				<?if ($arItem['UF_USER_ID_TO'] === CUser::GetID() && $arItem['UF_IS_DELETED_TO'] == false) :?>
					<li class="
						<?=(!$arItem['UF_IS_READ']) ? 'message-unread' : ''?>
						<?=($arItem['UF_TYPE'] == 'admin') ? 'important' : ''?> col-xs-12" 
						data-messid="<?=$arItem['ID']?>"
						data-itemid="<?=$arItem['UF_PRODUCT_ID']?>"
						data-senderid="<?=$arItem['UF_USER_ID_FROM']?>"
						data-isread="<?=$arItem['UF_IS_READ']?>">
						<div class="col-xs-10">
							<p class="sender-name col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<?$senerObject = CUser::GetByID($arItem['UF_USER_ID_FROM'])->Fetch()?>
								<span><?=$senerObject['NAME']?></span>
							</p>
							<p class="message-text col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<span><?=$arItem['UF_BODY']?></span>
							</p>
							<?$timestamp = explode(' ', $arItem['UF_TIMESTAMP'])?>
							<p class="message-date col-lg-3 col-md-3 col-sm-3 col-xs-12"><?=$timestamp[0]?></p>
						</div>
						<p class="message-remove col-xs-2"><i class="fa fa-trash-o" aria-hidden="true"></i></p>
					</li>
					<?$deletedMessages[] = $arItem['UF_IS_DELETED_TO']?>
				<?endif?>
			<?endforeach?>
			<?if (!array_search(0, $deletedMessages) && array_search(0, $deletedMessages) != 0 || empty($deletedMessages)) :?>
				<p class="no-messages">У вас нет сообщений</p>
			<?endif?>
		<?else :?>
			<p class="no-messages">У вас нет сообщений</p>
		<?endif?>	
	</ul>
</div>


<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  "",
  Array(
    "AREA_FILE_SHOW" => "file",
    "AREA_FILE_SUFFIX" => "inc",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/includes/banner.php"
  )
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>