<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;

$messages_block_id = 2;
$entity_data_class = GetEntityDataClass($messages_block_id);

if (!empty($_POST['senderId'])) {
	$messages_block_id = 2;
	$entity_data_class = GetEntityDataClass($messages_block_id);
	$rsData = $entity_data_class::getList(array(
	    'select' => array('*'),
	    "order" => array("ID" => "ASC"),
	    'filter' => array(
	    	'LOGIC' => 'OR',
	    	array('=UF_USER_ID_FROM' => $_POST['senderId']),
	    	array('=UF_USER_ID_TO' => $_POST['senderId'])
	    	)
	));	
	$arItems = array();
	while($el = $rsData->fetch()) {
	    $arItems[] = $el;
	}

	if ($_POST['isRead'] == false && !empty($_POST['messageId'])) {
		$query = "UPDATE user_messages SET UF_IS_READ=1 WHERE ID=" . $_POST['messageId'];
		$DB->Query($query);
	}
}
?>

<div class="popup-wrapper">
	<?foreach ($arItems as $arItem) :?>
		<?if ($arItem['UF_USER_ID_FROM'] == $_POST['senderId']) :?>
			<p class="mess-in col-xs-6"><?=$arItem['UF_BODY']?></p>
		<?else :?>	
			<p class="mess-out col-xs-6 col-xs-offset-6"><?=$arItem['UF_BODY']?></p>
		<?endif?>
		<div class="clearfix"></div>
	<?endforeach?>
	<div class="answer-block">
		<textarea rows="3"></textarea>
		<button>Отправить</button>
	</div>	
</div>

<script type="text/javascript">
	$('.answer-block button').on('click', function () {
	    let messageText = $(this).parent().find('textarea').val();
	    let senderId = "<?=CUser::GetID()?>";
	    let receiver = "<?=$_POST['senderId']?>";
	    let productId = "<?=$arItems[0]['UF_PRODUCT_ID']?>";
	    //$.fancybox.showLoading();
	    $.ajax({
	      url: '/account/messages/sendMessage.php',
	      data: {messageText: messageText, senderId: senderId, receiver: receiver, productId: productId},
	      method: 'POST',
	      success: function (response) {
			  $.ajax({
			    url: '/account/messages/getAllMessages.php',
			    data: {senderId: receiver},
			    method: 'POST',
			    success: function (response) {
			      $.fancybox.open({
			        content: response,
			      });
			    }
			  });
	      	}
	    });
  	})

</script>